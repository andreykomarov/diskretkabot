from enum import Enum
from typing import *

class State(Enum):
    default = "DEFAULT"
    registering = "REGISTERING"
    submitting = "SUBMITTING"
    modifying = "MODIFYING"
    status = "SHOWING_STATUS"
    feedback = "FEEDBACK"
    failing = "FAILING"
    switching = "SWITCHING"
    scheduling = "SCHEDULING"
    scores = "SCORES"
    chgrp = "CHGRP"
    useradd = "USERADD"

TgId = NewType("TgId", int)
StudentId = NewType("StudentId", int)
StudentName = NewType("StudentName", str)
CourseName = NewType("CourseName", str)
CourseId = NewType("CourseId", int)
CourseAlias = NewType("CourseAlias", str)
Course = NamedTuple("Course", [("name", CourseName), ("alias", str), ("id", CourseId)])
GroupId = NewType("GroupId", int)
GroupName = NewType("GroupName", str)
GroupPattern = NewType("GroupPattern", str)
Problem = NewType("Problem", int)
Timestamp = NewType("Timestamp", str)
Submit = NamedTuple("Submit", [
    ("name", StudentName),
    ("timestamp", Timestamp),
    ("score", int),
    ("studentid", StudentId),
    ("problems", List[Problem])])
SchedItem = NamedTuple("SchedItem", [
    ("studentid", StudentId),
    ("status", str), # TODO
    ("name", StudentName),
    ("problem", Problem),
    ("score", int)])
ScoreEntry = NamedTuple("ScoreEntry", [
    ("name", StudentName),
    ("score", int)])
ScoresCallback = Callable[[], List[Tuple[CourseId, StudentName, int]]]
UpdateRequest = NamedTuple("UpdateRequest", [
    ("tgid", TgId),
    ("data", str),
    ("json", Dict[str, Any])])
UpdateResponseT = NamedTuple("UpdateResponse", [
    ("text", str),
    ("state", State),
    ("keyboard", List[List[str]])])

def UpdateResponse(**kwargs: Any) -> UpdateResponseT:
    if not kwargs.get("keyboard"):
        kwargs["keyboard"] = None
    return UpdateResponseT(**kwargs)

class EdgeStatus(Enum):
    empty = "EMPTY"
    taken = "TAKEN"
    failed = "FAILED"
    #forced = "FORCED"
