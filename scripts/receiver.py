import pika
import json
import base64
import subprocess
import random
from typing import Dict, Any

import config
import api

import logging
import logging.handlers
handler = logging.handlers.SysLogHandler(address = "/dev/log")
log = logging.getLogger(config.NAME)
log.setLevel(logging.DEBUG)
log.addHandler(handler)

URL = "amqp://{}:{}@{}/".format(
    config.NAME, config.TOKEN, config.HOST)
UNPROCESSED = "unprocessed"
FAILED = "failed"
IMAGE_SRC = "/tmp/image-src.jpg"
IMAGE_DST = "/tmp/image-dst.png"
VIDEO_SRC = "/tmp/video-src.mp4"
VIDEO_DST = "/tmp/video-dst.mp4"

MODELS = [
    "eccv16/composition_vii.t7",
    "eccv16/la_muse.t7",
    "eccv16/starry_night.t7",
    "eccv16/the_wave.t7",
    "instance_norm/candy.t7",
    "instance_norm/feathers.t7",
    "instance_norm/la_muse.t7",
    "instance_norm/mosaic.t7",
    "instance_norm/the_scream.t7",
    "instance_norm/udnie.t7"
]

conn = pika.BlockingConnection(pika.URLParameters(URL))
channel = conn.channel()

import threading
def ping(conn):
    while True:
        conn.sleep(0.5)
        conn.process_data_events()
heartbeat = threading.Thread(target=ping, args=[conn])
heartbeat.start()

channel.queue_declare(UNPROCESSED, durable=True)
channel.queue_declare(FAILED, durable=True)

def process_image(data: Dict[str, Any]) -> None:
    image = base64.b64decode(data['data'].encode("ascii"))
    f = open(IMAGE_SRC, "wb")
    f.write(image)
    f.close()
    model = random.choice(MODELS)
    subprocess.check_call("cd ~/code/fast-neural-style; convert {} {}.png; ~/torch/install/bin/th fast_neural_style.lua -model models/{} -input_image {}.png -output_image {} -gpu 0".format(IMAGE_SRC, IMAGE_SRC, model, IMAGE_SRC, IMAGE_DST), shell=True)
    api.send_photo(data['tag'], IMAGE_DST)

def process_video(data: Dict[str, Any]) -> None:
    video = base64.b64decode(data['data'].encode("ascii"))
    f = open(VIDEO_SRC, "wb")
    f.write(video)
    f.close()
    subprocess.check_call("./convert.sh {} {}".format(VIDEO_SRC, VIDEO_DST), shell=True)
    api.send_video(data['tag'], VIDEO_DST)

def callback(ch, method, properties, body):
    try:
        data = json.loads(body.decode("ascii"))
        log.info(data["tag"])
        if data["type"] == "photo":
            process_image(data)
        elif data["type"] == "video":
            process_video(data)
        else:
            log.warning("Unknown type: {}".format(data["type"]))
        log.info(" [x] Done")
        ch.basic_ack(delivery_tag = method.delivery_tag)
    except Exception:
        log.exception("something bad")
        channel.basic_publish(exchange='',
                              routing_key=FAILED,
                              body=body,
                              properties=pika.BasicProperties(
                                  delivery_mode=2
                              ))
        ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback,
                      queue=UNPROCESSED)

channel.start_consuming()
