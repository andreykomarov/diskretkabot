#import pika
import json
import base64

import config
from logs import log

URL = "amqp://{}:{}@localhost/".format(
    config.NAME, config.TOKEN)
UNPROCESSED = "unprocessed"

def publish(tag: str, data: bytes, type: str) -> None:
    log.warning("Deprecated")
    #conn = pika.BlockingConnection(pika.URLParameters(URL))
    #channel = conn.channel()
    #channel.queue_declare(UNPROCESSED, durable=True)
    #body = json.dumps({
    #    "tag": tag,
    #    "data": base64.b64encode(data).decode("ascii"),
    #    "type": type
    #}).encode("ascii")
    #channel.basic_publish(exchange='',
    #                      routing_key=UNPROCESSED,
    #                      body=body,
    #                      properties=pika.BasicProperties(
    #                          delivery_mode=2
    #                      ))
    #channel.close()
    #conn.close()
