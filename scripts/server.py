import http.server
import ssl
import json
from typing import Dict, Any

import config
import db
import api
from dfa import automata
from common import *
from logs import *
import messages

def process_photo(data: Dict[str, Any]) -> None:
    tgid = data['message']['chat']['id']
    photo = api.get_file(data['message']['photo'][-1]['file_id'])
    messages.publish(tgid, photo, 'photo')

def process_video(data: Dict[str, Any]) -> None:
    tgid = data['message']['chat']['id']
    video = api.get_file(data['message']['video']['file_id'])
    messages.publish(tgid, video, 'video')

def process_sticker(data: Dict[str, Any]) -> None:
    tgid = data['message']['chat']['id']
    sticker = api.get_file(data['message']['sticker']['file_id'])
    messages.publish(tgid, sticker, 'photo')

def process_update(data: Dict[str, Any]) -> None:
    log.info("New update: {}".format(data))
    if "photo" in data['message']:
        process_photo(data)
    if "video" in data['message']:
        process_video(data)
    if "sticker" in data['message']:
        process_sticker(data)
    text = data['message']['text']
    tgid = data['message']['from']['id']
    #first_name = data['message']['from']['first_name']
    #last_name = data['message']['from']['last_name']
    first_name, last_name = "<none>", "<none>"
    #log.info("Received message <%s> from <%s>(aka %s %s)", text, tgid, first_name, last_name)
    state = db.get_state(tgid)
    req = UpdateRequest(tgid=tgid, data=text, json=data)
    for predicate, callback in automata[state]:
        if not predicate(req):
            continue
        log.info("Calling {} for {} {} {}".format(
            callback, req.tgid, first_name, last_name))
        resp = callback(req)
        db.set_state(req.tgid, resp.state)
        api.send_message(req.tgid, resp.text, resp.keyboard)
        break

class MyHandler(http.server.BaseHTTPRequestHandler):
    def do_POST(self) -> None:
        if config.TOKEN not in self.path:
            self.send_error(403)
            return
        try:
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.end_headers()
            req_len = int(self.headers['Content-Length'])
            data = self.rfile.read(req_len).decode('utf8')
            db.dump_request(data)
            m = json.loads(data)
            process_update(m)
#            self.wfile.write(json.dumps(response).encode('utf8'))
        except Exception:
            log.exception("something bad happened")

    
if __name__ == '__main__':
    httpd = http.server.HTTPServer(('', config.PORT), MyHandler)
    httpd.socket = ssl.wrap_socket(httpd.socket,
                                   keyfile=config.KEYFILE,
                                   certfile=config.CERTFILE,
                                   server_side=True)
    httpd.serve_forever()
