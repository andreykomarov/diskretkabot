import requests
import re
import random
import subprocess
import os
import sys
from typing import TypeVar, Sequence

from common import *
from logs import *
from config import *
import db
import api

def usage(req: UpdateRequest) -> UpdateResponseT:
    msg = """/reg <что-то, похожее на ваше имя> --- представиться системе
/unreg --- разлогиниться
/me --- получить информацию о себе
/scores --- посмотреть, у кого сколько баллов
/switch --- выбрать предмет (ДМ или АСД)
/submit <список задач, например, 1 2 5-9 11-19> --- отправить заявку
/mod <что как поменять, например, +2 -2> --- модифицировать последнюю заявку
/status <группа> --- показать, кто что заявил в зададнной группе
/chgrp <группа> --- переехать в другую группу
/help --- показать это сообщение
/feedback --- напишите нам что-нибудь"""
    if db.is_admin(req.tgid):
        msg += """
/refresh --- скачать баллы из гуглодоки. Достаточно делать раз в неделю
/sched <группа> --- построить распределение для группы
/fail <номер задачи> --- (экспериментально) перестроить распределение с учётом того, 
  что человек из предыдущего /sched не решил задачу
/useradd <имя> --- (экспериментально) добавить нового студента
/suicide --- ☠☠☠
"""
    return UpdateResponse(text = msg, state = State.default)

def register(req: UpdateRequest) -> UpdateResponseT:
    if req.data.startswith("/reg"):
        req = req._replace(data = " ".join(req.data.split(maxsplit=2)[1:]))
    if not req.data:
        return UpdateResponse(
            text = "Пожалуйста, представьтесь",
            state = State.registering
        )
    log.debug("reg {} {}".format(type(req.data), req.data))
    guesses = db.students_like(req.data)
    if len(guesses) == 0:
        return UpdateResponse(
            text = "Не удалось понять, кто вы. Попробуйте ещё раз",
            state = State.registering
        )
    elif len(guesses) > 10:
        return UpdateResponse(
            text = "Нашлось {} вариантов. Вот первые десять:\n{}".format(
                len(guesses), "\n".join(guesses[:10])),
            state = State.registering
        )
    elif len(guesses) > 1:
        return UpdateResponse(
            text = "Нашлось несколько вариантов. Вот они:\n{}".format(
                "\n".join(guesses)),
            state = State.registering
        )
    guess = guesses[0]
    db.successful_registration(req.tgid, guess)
    for tgid in db.tgids_with_studentname(guess):
        if tgid == req.tgid:
            continue
        def get(a: List[str]) -> str:
            try:
                res = req.json # type: Any
                for x in a:
                    res = res[x]
                return res
            except:
                return "<none>"
        tgusername = get(['message', 'from', 'username'])
        first_name = get(['message', 'from', 'first_name'])
        last_name = get(['message', 'from', 'last_name'])
        api.send_message(tgid, """
К И Б Е Р П Р Е С Т У П Л Е Н И Е:
кое-кто ({} {} aka @{}) залогинился под тем же именем, что и вы.
""".format(
    first_name, last_name, tgusername))

    return UpdateResponse(
        text = "Добро пожаловать, {}".format(guess),
        state = State.default
    )

def unregister(req: UpdateRequest) -> UpdateResponseT:
    db.unreg(req.tgid)
    return UpdateResponse(
        text = "Вы разрегистрированы",
        state = State.default
    )

def submit(req: UpdateRequest) -> UpdateResponseT:
    if req.data.startswith("/submit"):
        req = req._replace(data = " ".join(req.data.split(maxsplit=2)[1:]))
    if not req.data:
        return UpdateResponse(
            text = "Введите номера решённых заданий",
            state = State.submitting
        )
    s = set()
    count = 0
    for part in re.findall("[0-9-]+", req.data):
        if '-' not in part:
            count += 1
        else:
            left, right = map(int, part.split('-'))
            count += right - left + 1
    if count > 100:
        return UpdateResponse(
            text = "А не жирно {} задач заявлять? Попробуйте ещё раз.".format(count),
            state = State.submitting
        )

    for part in re.findall("[0-9-]+", req.data):
        if '-' not in part:
            s.add(int(part))
        else:
            left, right = map(int, part.split('-'))
            s.update(range(left, right + 1))
    problems_list = sorted(s)
    studentid = db.studentid_by_tgid(req.tgid)
    problems = ",".join(map(str,problems_list))
    courseid = db.get_selected_course(req.tgid)
    db.successful_submit(studentid, courseid, problems)
    if problems:
        return UpdateResponse(
            text = "Вы заявили задачи {}".format(problems),
            state = State.default
        )
    else:
        return UpdateResponse(
            text = "Вы отправили пустую заявку. Очень жаль.",
            state = State.default
        )

def permission_denied(req: UpdateRequest) -> UpdateResponseT:
    return UpdateResponse(
        text = "Недостаточно прав",
        state = State.default
    )

L = TypeVar('L')
R = TypeVar('R')
def kuhn(g: Dict[L, List[R]], weight: Dict[L, int], matching: Dict[R, L]) -> Dict[L, R]:
    left = list(weight.items())
    random.shuffle(left)
    used = {} # type: Dict[L, bool]
    def dfs(v: L) -> bool:
        if v in used:
            return False
        used[v] = True
        for u in g[v]:
            if (u not in matching) or dfs(matching[u]):
                matching[u] = v
                return True
        return False
    matched = set(matching.values())
    for v, w in sorted(left, key=lambda x: x[1], reverse=True):
        if v in matched:
            continue
        used.clear()
        dfs(v)
    res = {}
    for right, left_ in matching.items():
        res[left_] = right
    return res

def get_recent_submits(group: GroupPattern, courseid: CourseId) -> Dict[Tuple[StudentId, StudentName], List[Problem]]:
    recent_submits = db.recent_submits(group, courseid)
    log.debug("sched {}".format(recent_submits))
    times, solved = {}, {} # type: Dict[Tuple[StudentId, StudentName], Timestamp], Dict[Tuple[StudentId, StudentName], List[Problem]]
    for row in recent_submits:
        name, time = row.name, row.timestamp
        problems = row.problems
        score = int(row.score)
        studentid = row.studentid
        key = (studentid, name)
        if (key not in times) or (times[key] < time):
            times[key] = time
            random.shuffle(problems)
            solved[key] = problems
    log.debug("solved = {}".format(solved))
    return solved

def sched(req: UpdateRequest) -> UpdateResponseT:
    if req.data.startswith("/sched"):
        req = req._replace(data = " ".join(req.data.split(maxsplit=2)[1:]))
    if not req.data:
        return UpdateResponse(
            text = "Для какой группы построить распределение?",
            keyboard = partition(db.groups_and_aliases()),
            state = State.scheduling
        )
    group = GroupPattern(req.data.split()[0])
    courseid = db.get_selected_course(req.tgid)
    solved = get_recent_submits(group, courseid)
    db.create_sched_template(req.tgid, solved)
    return show_sched(req)

def show_sched(req: UpdateRequest) -> UpdateResponseT:
    courseid = db.get_selected_course(req.tgid)
    sched = db.get_last_schedule(req.tgid, courseid)
    Key = Tuple[StudentId, StudentName]
    solved, scores, failed = {}, {}, {} # type: Dict[Key, List[Problem]], Dict[Key, int], Dict[Key, List[Problem]]
    matching = {}
    for row in sched:
        key = (row.studentid, row.name)
        if key not in solved:
            solved[key] = []
        status = row.status
        problem = row.problem
        if status == EdgeStatus.empty.value:
            solved[key].append(problem)
        elif status == EdgeStatus.failed.value:
            if key not in failed:
                failed[key] = []
            failed[key].append(problem)
        elif status == EdgeStatus.taken.value:
            solved[key].append(row.problem)
            matching[problem] = key
        scores[key] = row.score
    weight = {key : len(problems) - scores[key] - (100 if key in failed else 0)
              for key, problems in solved.items()}
    schedule = kuhn(solved, weight, matching)
    schedule_by_problem_dict = {prob : name
                                for (_, name), prob in schedule.items()}
    schedule_by_problem = sorted(schedule_by_problem_dict.items())
    def get_status(row: SchedItem) -> str:
        key = (row.studentid, row.name)
        problem = row.problem
        if problem in failed.get(key, []):
            return EdgeStatus.failed.value
        if schedule.get(key) == problem:
            return EdgeStatus.taken.value
        return EdgeStatus.empty.value
    schedule_to_db = [(req.tgid, row.problem, row.studentid, get_status(row))
                      for row in sched]
    db.replace_last_schedule(req.tgid, schedule_to_db)
    return UpdateResponse(
        text = str(schedule_by_problem).replace("[", "").replace("]", ""),
        state = State.default
    )

def refresh(req: UpdateRequest) -> UpdateResponseT:
    import csv
    import io
    for getter in GDOC_CSV:
        for courseid, name, score in getter():
            db.set_student_score(name, courseid, score)
    return UpdateResponse(
        text = "Данные успешно скачаны",
        state = State.default
    )
    
def register_first(req: UpdateRequest) -> UpdateResponseT:
    return UpdateResponse(
        text = "Сначала нужно зарегистрироваться",
        state = State.default
    )

def try_help(req: UpdateRequest) -> UpdateResponseT:
    print(db.get_courses())
    return UpdateResponse(
        text = "Попробуйте /help",
        state = State.default
    )

def status(req: UpdateRequest) -> UpdateResponseT:
    if req.data.startswith("/status"):
        req = req._replace(data = " ".join(req.data.split(maxsplit=2)[1:]))
    if not req.data:
        return UpdateResponse(
            text = "Для какой группы построить табличку?",
            keyboard = partition(db.groups_and_aliases()),
            state = State.status
        )
    group = GroupPattern(req.data)
    courseid = db.get_selected_course(req.tgid)
    solved = get_recent_submits(group, courseid)
    problems = sorted(set(x for xs in solved.values() for x in xs))
    names = sorted(x for x in solved.keys())
    log.debug("names = {}, problems = {}".format(names, problems))
    log.debug("solved = {}".format(solved))
    log.debug("cwd = {}".format(os.getcwd()))
    log.debug("path = {}".format(os.getenv("PATH")))
    table_file = open("table.org", 'w', encoding="utf-8")
    head = "|Имя|Баллы|{}|".format("|".join(str(problem) for problem in problems))
    print(head, file=table_file)
    head2 = "|-{}-|".format("-".join("+" for _ in problems))
    print(head2, file=table_file)
    for name in names:
        score = db.score_by_studentid(name[0], CourseId(1))
        line = "|{}|{}|{}|".format(
            name[1], score,
            "|".join("+" if problem in solved[name] else " "
                     for problem in problems)
        )
        print(line, file=table_file)
    table_file.close()
    html = subprocess.check_call(
        ["pandoc", "table.org", "-c", "table.css", "-s", "-o", "table.html"]
    )
#    tmp_file = open("tabletmp.html", "r", encoding="utf-8")
#    out_file = open("table.html", "w", encoding="utf-8")
#    tmp_file_strings = tmp_file.readlines()
#    out_file.close()
    
    	
    if sys.platform != "win32":
        subprocess.check_call(
            ["xvfb-run", "phantomjs", "screenshot.js", "table.html", "table.png"]
    )
    else:
        subprocess.check_call(
            ["phantomjs", "screenshot.js", "table.html", "table.png"]
    )
    api.send_photo(req.tgid, "table.png")

    return UpdateResponse(
        text = "Таблица выслана",
        state = State.default
    )

def describe(req: UpdateRequest) -> UpdateResponseT:
    if not db.is_registered(req.tgid):
        return UpdateResponse(
            text = "Вы не зарегистрированы",
            state = State.default
        )
    studentid = db.studentid_by_tgid(req.tgid)
    courseid = db.get_selected_course(req.tgid)
    course = db.get_courses(courseid)[0]
    score = db.score_by_studentid(studentid, CourseId(1))
    name = db.name_by_studentid(studentid)
    group = db.group_by_studentid(studentid)
    problems = db.get_problems_for_tgid(req.tgid)
    return UpdateResponse(
        text = """Выбран предмет {}.\n{} ({}), {} баллов за выходы к доске.
Последняя заявка: {}""".format(
            course.name, name, group, score, ", ".join(map(str, problems))),
        state = State.default
    )

def modify(req: UpdateRequest) -> UpdateResponseT:
    if req.data.startswith("/mod"):
        req = req._replace(data = " ".join(req.data.split(maxsplit=2)[1:]))
    if not req.data:
        return UpdateResponse(
            text = "Что поменять?",
            state = State.modifying
        )
    problems_set = set(db.get_problems_for_tgid(req.tgid))
    problems_backup = sorted(list(problems_set))

    for part in re.findall("[-+][0-9]+", req.data):
        if part.startswith("-"):
            problems_set.discard(Problem(int(part[1:])))
        elif part.startswith("+"):
            problems_set.add(Problem(int(part[1:])))

    log.debug("Problems: {} {}".format(problems_set, problems_backup))
    if sorted(problems_set) == problems_backup:
        return UpdateResponse(
            text = "Но ведь ничего не поменялось, что это было?",
            state = State.default
        )

    if len(problems_set) > 100:
        return UpdateResponse(
            text = "Ну грешно столько задач заявлять",
            state = State.modifying
        )

    studentid = db.studentid_by_tgid(req.tgid)
    problems = ",".join(map(str,sorted(problems_set)))
    courseid = db.get_selected_course(req.tgid)
    db.successful_submit(studentid, courseid, problems)
    if problems:
        return UpdateResponse(
            text = "Заявка изменена на {}".format(problems),
            state = State.default
        )
    else:
        return UpdateResponse(
            text = "Теперь заявка пустая :(",
            state = State.default
        )
    
def feedback(req: UpdateRequest) -> UpdateResponseT:
    if req.data.startswith("/feedback"):
        req = req._replace(data = " ".join(req.data.split(maxsplit=2)[1:]))
    if not req.data:
        return UpdateResponse(
            text = "Вам есть, что сказать?",
            state = State.feedback
        )
    db.save_feedback(req.tgid, req.data)
    for tgid in db.get_admins():
        api.send_message(tgid, "feedback от {}: {}".format(
            api.describe(req),
            req.data)
        )
    return UpdateResponse(
        text = "Спасибо. Ваше мнение очень важно для нас",
        state = State.default
    )

def fail(req: UpdateRequest) -> UpdateResponseT:
    if req.data.startswith("/fail"):
        req = req._replace(data = " ".join(req.data.split(maxsplit=2)[1:]))
    if not req.data:
        return UpdateResponse(
            text = "Какую задачу не рассказали?",
            state = State.failing
        )
    try:
        problem = Problem(int(req.data))
    except ValueError:
        return UpdateResponse(
            text = "Вы ввели какую-то муть",
            state = State.default
        )	
    db.set_failed(req.tgid, problem)
    return show_sched(req)

def switch(req: UpdateRequest) -> UpdateResponseT:
    courses = db.get_courses()
    if req.data.startswith("/switch"):
        req = req._replace(data = " ".join(req.data.split(maxsplit=2)[1:]))
    if not req.data:
        return UpdateResponse(
            text = "Выберите предмет:\n" + "\n".join(c.name for c in courses),
            keyboard = [[c.alias for c in courses]],
            state = State.switching
        )
    rows = db.get_courses(req.data)
    if not rows:
        return UpdateResponse(
            text = "Попробуйте ещё раз:\n" +
                   "\n".join(c.name for c in courses),
            keyboard = [[c.alias for c in courses]],
            state = State.switching
        )
    row = rows[0]
    db.save_course(req.tgid, row.id)
    return UpdateResponse(
        text = "Вы выбрали предмет {}".format(row.name),
        state = State.default
    )

T = TypeVar('T')
def partition(texts: Sequence[T]) -> List[Sequence[T]]:
    res = []
    while texts:
        res.append(texts[:3])
        texts = texts[3:]
    return res

def format_table(items_any: Sequence[Sequence[T]], format: str = None) -> str:
    if not items_any:
        return ":("
    items = [["{}".format(x) for x in row] for row in items_any]
    lens = [max(len(items[i][j]) for i in range(len(items)))
            for j in range(len(items[0]))]
    res = []
    for row in items:
        here = []
        for i, item in enumerate(row):
            pad = lens[i] - len(item)
            if format and format[i] == 'r':
                item = ' ' * pad + item
            else:
                item = item + ' ' * pad
            here.append(item)
        res.append(" ".join(here))
    return "\n".join(res)

def scores(req: UpdateRequest) -> UpdateResponseT:
    if req.data.startswith("/scores"):
        req = req._replace(data = " ".join(req.data.split(maxsplit=2)[1:]))
    if not req.data:
        return UpdateResponse(
            text = "Для какой группы показать баллы?",
            keyboard = partition(db.groups_and_aliases()),
            state = State.scores
        )
    group = GroupPattern(req.data)
    students = db.scores_by_group(req.tgid, group)
    items = [(s.name, s.score) for s in students]

    return UpdateResponse(
        text = "```{}```".format(format_table(items, "rl")),
        state = State.default
    )

def chgrp(req):
    if req.data.startswith("/chgrp"):
        req = req._replace(data = " ".join(req.data.split(maxsplit=2)[1:]))
    if not req.data:
        return UpdateResponse(
            text = "В какую группу вы хотите перейти?",
            keyboard = partition(db.groups()),
            state = State.chgrp
        )
    group = req.data
    sid = db.studentid_by_tgid(req.tgid)
    assert sid
    gid = db.get_gid(req.data)
    if not gid:
        return UpdateResponse(
            text = "Такой группы нет, попробуйте ещё раз",
            state = State.chgrp
        )
    db.set_group(sid, gid)
    return UpdateResponse(
        text = "Успех! Вы теперь в группе {}".format(req.data),
        state = State.default
    )

def useradd(req):
    if req.data.startswith("/useradd"):
        req = req._replace(data = " ".join(req.data.split(maxsplit=2)[1:]))
    if not req.data:
        return UpdateResponse(
            text = "Кого хотите добавить? (Или /cancel, чтобы не добавлять никого)",
            state = State.useradd
        )
    sname = StudentName(req.data)
    db.useradd(sname)
    return UpdateResponse(
        text = "Пользователь '{}' добавлен".format(sname),
        state = State.default
    )

def useradd_cancel(req):
    return UpdateResponse(
        text = "Ок, никого не добавлено",
        state = State.default
    )

def suicide(req):
    api.send_message(req.tgid, "☠☠☠")
    import sys
    sys.exit(0)
