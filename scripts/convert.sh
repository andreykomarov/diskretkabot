#!/bin/bash

TMPIN=/tmp/vid-in
TMPOUT=/tmp/vid-out
TMPAUDIO=/tmp/snd.mp3
rm -rf $TMPIN $TMPOUT
mkdir $TMPIN
mkdir $TMPOUT
ffmpeg -i "$1" "$TMPIN/file%04d.png"
ffmpeg -i "$1" "$TMPAUDIO"
pushd ~/code/fast-neural-style
~/torch/install/bin/th fast_neural_style.lua -model models/eccv16/starry_night.t7 -input_dir $TMPIN -output_dir $TMPOUT -gpu 0
popd
rm -f "$2" "$2.mpg"
ffmpeg -i "$TMPOUT/file%04d.png" -i "$TMPAUDIO" "$2.mpg"
ffmpeg -i "$2.mpg" "$2"


