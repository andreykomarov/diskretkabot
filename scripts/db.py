import sqlite3
import re
from typing import Sequence, Optional, Tuple, Union

from config import DB
from common import *
from logs import *

def regexp(expr: str, item: str) -> bool:
    reg = re.compile(expr)
    return reg.search(item) is not None

conn = sqlite3.connect(DB)
conn.row_factory = sqlite3.Row
conn.create_function("REGEXP", 2, regexp)

def dump_request(data: str) -> None:
    conn.execute("INSERT INTO requests(data) VALUES (?)", (data,))
    conn.commit()

def is_admin(tgid: TgId) -> bool:
    cur = conn.execute("SELECT * FROM is_admin WHERE tgid = ?", (tgid,))
    row = cur.fetchone()
    return row != None

def is_registered(tgid: TgId) -> bool:
    cur = conn.execute("""
SELECT * FROM tgid_to_studentid
WHERE tgid = ?
""",
                       (tgid,))
    return cur.fetchone() != None

def get_state(tgid: TgId, default: State = State.default) -> State:
    cur = conn.execute("SELECT state FROM state WHERE tgid = ?", (tgid,))
    row = cur.fetchone()
    if row:
        return State(row[0])
    else:
        conn.execute("INSERT INTO state(tgid,state) VALUES (?, ?)",
                     (tgid, default.value))
        conn.commit()
        return default

def set_state(tgid: TgId, state: State) -> None:
    conn.execute("INSERT INTO state(tgid, state) VALUES (?, ?)",
                 (tgid, state.value))
    conn.commit()

def successful_registration(tgid: TgId, name: StudentName) -> None:
    conn.execute("""
INSERT INTO tgid_to_studentid 
VALUES (
    ?, 
    (SELECT rowid FROM students WHERE name = ?)
)""",
                 (tgid, name))
    conn.commit()

def students_like(data: str) -> List[StudentName]:
    cur = conn.execute("SELECT name FROM students");
    names = [x[0] for x in cur.fetchall()]
    parts = data.split()
    matches = [name for name in names if data.strip().lower() == name.lower()]
    if not matches:
        matches = [name
                   for name in names
                   if all(part.lower() in name.lower() for part in parts)]
        log.debug("Inexact match 1: {}".format(matches))
    if not matches:
        matches = [name
                   for name in names
                   if any(part.lower() in name.lower() for part in parts)]
        log.debug("New matches: {}".format(matches))
    return matches

def studentid_by_tgid(tgid: TgId) -> Optional[StudentId]:
    cur = conn.execute("SELECT studentid FROM tgid_to_studentid WHERE tgid = ?", (tgid,))
    row = cur.fetchone()
    if row:
        return StudentId(row[0])
    else:
        return None

def successful_submit(studentid: StudentId,
                      courseid: CourseId,
                      problems: str) -> None:
    log.debug("successful_submit {} {}".format(studentid, problems))
    conn.execute("""
INSERT INTO solved_problems(studentid, courseid, problems)
VALUES (?, ?, ?)
""",
                 (studentid, courseid, problems))
    conn.commit()
    log.debug("successful_submit done")

def score_by_studentid(studentid: StudentId,
                       courseid: CourseId) -> Optional[int]:
    cur = conn.execute("""
SELECT score FROM scores
WHERE studentid = ?
""",
                       (studentid,))
    row = cur.fetchone()
    if row:
        return row[0]
    else:
        return None

def name_by_studentid(studentid: StudentId) -> Optional[StudentName]:
    cur = conn.execute("""
SELECT name FROM students
WHERE rowid = ?
""",
                       (studentid,))
    row = cur.fetchone()
    if row:
        return row[0]
    else:
        return None

def group_by_studentid(studentid: StudentId) -> Optional[GroupName]:
    cur = conn.execute("""
SELECT groups.name FROM groups
    JOIN students ON students.groupid = groups.rowid
WHERE students.rowid = ?
""",
                       (studentid,))
    row = cur.fetchone()
    if row:
        return row[0]
    else:
        return None

def set_student_score(name: StudentName,
                      courseid: CourseId,
                      score: int) -> None:
    cur = conn.execute("SELECT rowid FROM students WHERE name LIKE ?",
                       ('%{}%'.format(name),))
    row = cur.fetchone()
    if not row:
        log.warning("Student not found: {}".format(name))
        return
    conn.execute("""
INSERT INTO scores
VALUES (?, ?, ?)""",
                 (row[0], courseid, score))
    conn.commit()

def recent_submits(group: GroupPattern,
                   courseid: CourseId) -> Sequence[Submit]:
    log.debug("recent_submits {} {}".format(group, courseid))
    recent_requests = conn.execute("""
SELECT * FROM solved_problems
    JOIN students ON solved_problems.studentid = students.rowid
    NATURAL JOIN scores
    JOIN groups ON groups.rowid = groupid 
WHERE 
    timestamp > datetime('now', '-3 days')
    AND groups.name REGEXP ?
    AND courseid = ?
""",
                                   ("{}".format(group),courseid))
    result = [Submit(
        name=row[4],
        timestamp=row[3],
        score=row[6],
        studentid=row[0],
        problems=[Problem(int(x)) for x in row[2].split(',') if x])
              for row in recent_requests.fetchall()]
    log.debug("recent_submits res {}".format(result))
    return result

def tgids_with_studentname(studentname: StudentName) -> List[TgId]:
    cur = conn.execute("""
SELECT tgid FROM tgid_to_studentid
    JOIN students ON students.rowid = tgid_to_studentid.studentid
WHERE students.name = ?
""",
                       (studentname,))
    return [id[0] for id in cur.fetchall()]

def unreg(tgid: TgId) -> None:
    conn.execute("DELETE FROM tgid_to_studentid WHERE tgid = ?",
                 (tgid,)
    )

def get_problems_for_tgid(tgid: TgId) -> List[Problem]:
    cur = conn.execute("""
SELECT problems FROM 
    tgid_to_studentid 
    NATURAL JOIN solved_problems 
WHERE tgid = ? 
ORDER BY timestamp DESC 
LIMIT 1;""",
                       (tgid,))
    row = cur.fetchone()
    if not row:
        return []
    if row[0]:
        problems = sorted(Problem(int(p)) for p in row[0].split(","))
    else:
        problems = []
    return problems

def save_feedback(tgid: TgId, text: str) -> None:
    conn.execute("""
INSERT INTO feedback(tgid, text) VALUES (?, ?)""",
                 (tgid, text))
    conn.commit()

def get_admins() -> List[TgId]:
    cur = conn.execute("SELECT tgid FROM is_admin")
    return [TgId(admin[0]) for admin in cur.fetchall()]

def create_sched_template(tgid: TgId, edges: Dict[Tuple[StudentId, StudentName], List[Problem]]) -> None:
    log.debug("edges: {}".format(edges))
    values = [(tgid, prob, sid, EdgeStatus.empty.value)
              for (sid, _), v in edges.items() for prob in v]
    log.debug("values: {}".format(values))
    cur = conn.cursor()
    cur.execute("DELETE FROM schedules WHERE tgid = ?", (tgid,))
    cur.executemany("INSERT INTO schedules VALUES (?, ?, ?, ?)", values)
    cur.close()
    conn.commit()

def get_last_schedule(tgid: TgId,
                      courseid: CourseId) -> List[SchedItem]:
    cur = conn.execute("""
SELECT * FROM schedules
    NATURAL JOIN scores
    JOIN students ON students.rowid = studentid
WHERE tgid = ? AND courseid = ?""",
                       (tgid, courseid))
    schedule = cur.fetchall()
    return [SchedItem(
        studentid=row[2],
        status=row[3],
        problem=row[1],
        score=row[5],
        name=row[6]
    ) for row in schedule]

def replace_last_schedule(
        tgid: TgId,
        schedule: List[Tuple[TgId, Problem, StudentId, str]]) -> None:
    cur = conn.cursor()
    cur.execute("DELETE FROM schedules WHERE tgid = ?", (tgid,))
    log.debug("sched = {}".format(schedule))
    cur.executemany("INSERT INTO schedules VALUES (?, ?, ?, ?)",
                     schedule)
    cur.close()
    conn.commit()

def set_failed(tgid: TgId, problem: Problem) -> None:
    conn.execute("""
UPDATE schedules
SET status = ?
WHERE tgid = ? AND problem = ? AND status = ?""",
                 (EdgeStatus.failed.value, tgid,
                  problem, EdgeStatus.taken.value))
    conn.commit()

def get_courses(alias: Union[str, CourseId] = "") -> List[Course]:
    cur = conn.execute("""
SELECT name, alias, rowid FROM courses
WHERE alias LIKE ? OR rowid = ?""",
                       ("%{}%".format(alias), alias))
    return [Course(id=c[2],
                   name=c[0],
                   alias=c[1])
            for c in cur.fetchall()]

def save_course(tgid: TgId, courseid: CourseId) -> None:
    conn.execute("INSERT INTO selected_courses VALUES (?, ?)",
                 (tgid, courseid))
    conn.commit()

def get_selected_course(tgid: TgId) -> CourseId:
    cur = conn.execute("""
SELECT courseid FROM selected_courses
WHERE tgid = ?""",
                       (tgid,))
    row = cur.fetchone()
    if row:
        return CourseId(row[0])
    return CourseId(1)

def groups_and_aliases() -> Sequence[GroupPattern]:
    groups_rows = conn.execute("SELECT name FROM groups").fetchall()
    groups = [GroupPattern(g[0]) for g in groups_rows]
    aliases_rows = conn.execute("SELECT alias FROM group_aliases").fetchall()
    aliases = [GroupPattern(g[0]) for g in aliases_rows]
    return sorted(groups + aliases)

def groups() -> Sequence[GroupName]:
    groups = conn.execute("SELECT name FROM groups").fetchall()
    groups = [g[0] for g in groups]
    return sorted(groups)

def scores_by_group(tgid: TgId, group: GroupPattern) -> List[ScoreEntry]:
    course = get_selected_course(tgid)
    cur = conn.execute("""
SELECT * FROM scores
    JOIN students ON studentid = students.rowid
    JOIN groups ON students.groupid = groups.rowid
WHERE groups.name REGEXP ?
    AND courseid = ?
ORDER BY students.name""",
                       (group, course))
    return [ScoreEntry(
        name=row[3],
        score=row[2]) for row in cur.fetchall()]

def get_gid(group: GroupName) -> GroupId:
    cur = conn.execute("""SELECT rowid FROM groups WHERE name = ?""",
                       (group,))
    row = cur.fetchone()
    if row:
        return GroupId(row[0])
    return None

def set_group(sid: StudentId, gid: GroupId) -> None:
    conn.execute("""
UPDATE students 
SET groupid = ?
where rowid = ?""",
                 (gid, sid))
    conn.commit()

def useradd(sname: StudentName) -> None:
    conn.execute("""INSERT INTO students(name) VALUES (?)""", (sname,))
    conn.commit()
