PORT = 8443
NAME = "diskretkabot"
HOST = "akomarov.org"
KEYFILE = "./{}.key".format(NAME)
CERTFILE= "./{}.pem".format(NAME)
DB = "test.db"
TOKEN = open("token").read().strip()
API = "https://api.telegram.org/bot{}/".format(TOKEN)
API_FILE = "https://api.telegram.org/file/bot{}/".format(TOKEN)

from common import GroupPattern, CourseAlias

from gdocdownloader import byname, ads3
GDOC_CSV = [
    byname(
        "https://docs.google.com/spreadsheets/d/1EWGI1L5oOx7lS0r2vxT8HHIup75z9grmSGvzgPR6Lhc/export?format=csv&id=1EWGI1L5oOx7lS0r2vxT8HHIup75z9grmSGvzgPR6Lhc&gid=0",
        GroupPattern("M313."),
        CourseAlias("ДМ")
    ),
    byname(
        "https://docs.google.com/spreadsheets/d/1CVst4sANNQsbVcuYJmXgkxASalz98qZ6NMN3Ae3MnuA/export?format=csv&id=1CVst4sANNQsbVcuYJmXgkxASalz98qZ6NMN3Ae3MnuA&gid=0",
        GroupPattern("M323[89]"),
        CourseAlias("АСД"),
        "Семинары"
    ),
    ads3(
        "https://docs.google.com/spreadsheets/d/1CVst4sANNQsbVcuYJmXgkxASalz98qZ6NMN3Ae3MnuA/export?format=csv&id=1CVst4sANNQsbVcuYJmXgkxASalz98qZ6NMN3Ae3MnuA&gid=1916280332",
        GroupPattern("M323[4567]"),
        CourseAlias("АСД")
    ),
    byname(
        "https://docs.google.com/spreadsheets/d/1UmUT5xAb0cSZjDL5apcamP97rSSKnVeE-oKlANfBVXA/export?format=csv&id=1UmUT5xAb0cSZjDL5apcamP97rSSKnVeE-oKlANfBVXA&gid=0",
        GroupPattern("M323."),
        CourseAlias("ДМ")
    ),
    byname(
        "https://docs.google.com/spreadsheets/d/1SRuaR5LAsiEK4lwHmGG1UxsA9_2xJ0zZUEa4zWU_kGw/export?format=csv&id=1SRuaR5LAsiEK4lwHmGG1UxsA9_2xJ0zZUEa4zWU_kGw&gid=0",
        GroupPattern("M313[89]"),
        CourseAlias("АСД"),
        "Семинары"
    ),
    ads3(
        "https://docs.google.com/spreadsheets/d/1SRuaR5LAsiEK4lwHmGG1UxsA9_2xJ0zZUEa4zWU_kGw/export?format=csv&id=1SRuaR5LAsiEK4lwHmGG1UxsA9_2xJ0zZUEa4zWU_kGw&gid=1916280332",
        GroupPattern("M313[4567]"),
        CourseAlias("АСД")
    ),
    byname(
        "https://docs.google.com/spreadsheets/d/1SRuaR5LAsiEK4lwHmGG1UxsA9_2xJ0zZUEa4zWU_kGw/export?format=csv&id=1SRuaR5LAsiEK4lwHmGG1UxsA9_2xJ0zZUEa4zWU_kGw&gid=1675925088",
        GroupPattern("M313[23]"),
        CourseAlias("АСД"),
        "Ответы у доски"
    )
]
