from typing import Callable

from operations import *
import db

Pred = Callable[[UpdateRequest], bool]
startswith = lambda pref: lambda req: req.data.startswith(pref)
fand = lambda f, g: lambda *args: f(*args) and g(*args)
fnot = lambda f: lambda *args: not f(*args)
always = lambda *args: True
is_admin = lambda req: db.is_admin(req.tgid)
is_registered = lambda req: db.is_registered(req.tgid)

automata = {
    State.default: [
        (startswith("/help"), usage),
        (startswith("/reg"), register),
        (fand(is_registered, startswith("/submit")), submit),
        (fand(fnot(is_registered), startswith("/submit")), register_first),
        (fand(is_registered, startswith("/switch")), switch),
        (fand(fnot(is_registered), startswith("/switch")), register_first),
        (fand(fnot(is_registered), startswith("/submit")), register_first),
        (fand(is_registered, startswith("/mod")), modify),
        (fand(fnot(is_registered), startswith("/mod")), register_first),
        (fand(is_admin, startswith("/sched")), sched),
        (fand(fnot(is_admin), startswith("/sched")), permission_denied),
        (fand(is_admin, startswith("/fail")), fail),
        (fand(fnot(is_admin), startswith("/fail")), permission_denied),
        (startswith("/refresh"), refresh),
        (startswith("/status"), status),
        (startswith("/unreg"), unregister),
        (startswith("/me"), describe),
        (startswith("/feedback"), feedback),
        (startswith("/scores"), scores),
        (startswith("/chgrp"), chgrp),
        (fand(is_admin, startswith("/useradd")), useradd),
        (fand(is_admin, startswith("/suicide")), suicide),
        (always, try_help)
    ],
    State.registering: [
        (always, register)
    ],
    State.submitting: [
        (is_registered, submit),
        (always, register_first)
    ],
    State.modifying: [
        (is_registered, modify),
        (always, register_first)
    ],
    State.status: [
        (always, status)
    ],
    State.feedback: [
        (always, feedback)
    ],
    State.failing: [
        (always, fail)
    ],
    State.switching: [
        (always, switch)
    ],
    State.scheduling: [
        (always, sched)
    ],
    State.scores: [
        (always, scores)
    ],
    State.chgrp: [
        (is_registered, chgrp),
        (always, register_first)
    ],
    State.useradd: [
        (startswith("/cancel"), useradd_cancel),
        (always, useradd)
    ]
} # type: Dict[State, List[Tuple[Pred, Callable[[UpdateRequest], Any]]]]
